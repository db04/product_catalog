package com.example.sapient.productcatalog;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.example.sapient.productcatalog.exception.ApplicationException;
import com.example.sapient.productcatalog.model.Item;
import com.example.sapient.productcatalog.model.Product;
import com.example.sapient.productcatalog.model.ProductDTO;
import com.example.sapient.productcatalog.repository.ItemRepository;
import com.example.sapient.productcatalog.repository.ProductRepository;
import com.example.sapient.productcatalog.service.ProductService;
@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {
	@Mock
	ProductRepository repo;

	@Mock
	ItemRepository itemRepo;

	@InjectMocks
	ProductService productService;

	@Before
	public void setup() {

	}

	@Test
	public void testSaveItem() {
		Item item = Item.builder().price(5.0).supplierId("supplierId").build();
		when(itemRepo.save(item)).thenReturn(Item.builder().price(5.0).supplierId("supplierId").itemId(1).build());
		Integer itemId = productService.saveItem(item);
		assertEquals(1, itemId);
	}
	
	@Test
	public void testFindAllProductByCategory() {
		
	}
	
	@Test
	public void testFindByCode() {
		Integer code = 1;
		List<Item> itemList = new ArrayList<>();
		itemList.add(Item.builder().price(5.0).supplierId("supplierId1").itemId(11).productCode(1).build());
		itemList.add(Item.builder().price(7.0).supplierId("supplierId2").itemId(12).productCode(1).build());
		
		when(repo.findByProductCode(code)).thenReturn(Product.builder().brand("Levi").category("Jeans").
				color("black").productCode(1).productName("Denim Jeans").size("30").build());
		
		when(itemRepo.findByProductCode(code)).thenReturn(itemList);
		
		ProductDTO prodDto = productService.findByCode(code);
		assertEquals("black", prodDto.getProduct().getColor());
		assertEquals("Denim Jeans", prodDto.getProduct().getProductName());
		assertEquals(2, prodDto.getItemList().size());
	}
	
	@Test
	public void testUpdateSuccess() {
		Product requestedProduct = Product.builder().brand("Levi").category("Jeans").
				color("black").productCode(1).productName("Denim Jeans").size("30").build();
		when(repo.save(requestedProduct)).thenReturn(requestedProduct);
		when(repo.findByProductCode(1)) .thenReturn(requestedProduct);
		Integer productCode=productService.update(requestedProduct);
		assertEquals(requestedProduct.getProductCode(), productCode);
		
	}
	
	@Test(expected = ApplicationException.class)
	public void testUpdateException() {
		Product requestedProduct = Product.builder().brand("Levi").category("Jeans").color("black")
				.productName("Denim Jeans").size("30").build();
		productService.update(requestedProduct);

	}
}
