package com.example.sapient.productcatalog.model;

import lombok.Data;

@Data
public class Inventory {	
	String market;
	String supplierId;
	String sku;
	Long quantity;
}
