package com.example.sapient.productcatalog.model;

import java.util.List;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;

@Builder
@Data
@Getter
public class ProductDTO {
	
	List<Item> itemList ;
	Product product;

}
