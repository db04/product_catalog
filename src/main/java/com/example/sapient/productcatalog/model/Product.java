package com.example.sapient.productcatalog.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Builder
@Data
@Entity
@EqualsAndHashCode
@AllArgsConstructor
@ToString
@Getter
@Table(uniqueConstraints={@UniqueConstraint(columnNames = {"productName" , "category","brand","color","size"})})
public class Product {
	public Product() {
		super();
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Integer productCode;
	String productName;
	String category;
	String brand;
	String color;
	String size;	

}
