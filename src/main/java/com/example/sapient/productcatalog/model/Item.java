package com.example.sapient.productcatalog.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;

@Data
@Entity
@Getter
@Builder
public class Item {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Integer itemId;
	@NotNull
	Integer productCode;
	String supplierId;
	Double price;
	String sku;

}
