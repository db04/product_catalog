package com.example.sapient.productcatalog.model;

import lombok.Data;

@Data
public class Supplier {
	String supplierName;
	String supplierId;
	String supplierAdd;	

}
