package com.example.sapient.productcatalog.model;

import javax.validation.constraints.NotNull;

import org.springframework.http.HttpStatus;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class ErrorObject {
	@NotNull
	String errorMessage;
	@NotNull
	HttpStatus responseCode;
}
