package com.example.sapient.productcatalog.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.sapient.productcatalog.model.Item;
import com.example.sapient.productcatalog.model.Product;
import com.example.sapient.productcatalog.model.ProductDTO;
import com.example.sapient.productcatalog.service.ProductService;

@RestController
@RequestMapping(value="/product")
public class ProductController {
	@Autowired
	ProductService prodService;
	
	@PostMapping
	public ResponseEntity<Integer> saveProduct(@RequestBody Product productRequest) {
		return new ResponseEntity<>(prodService.create(productRequest),HttpStatus.CREATED);				
	}
	
	@PutMapping
	public Integer updateProduct(@RequestBody Product productRequest) {
		return prodService.update(productRequest);				
	}
	
	@PostMapping("/item")
	public ResponseEntity<Integer> saveItem(@RequestBody Item item) {
		return new ResponseEntity<>(prodService.saveItem(item),HttpStatus.CREATED);	
	}
	
	@GetMapping("/all")
	public List<Product> listAllProductByCategory(@RequestParam String category) {
		return prodService.findAllProdByCategory(category);
	}
	
	@GetMapping
	public ProductDTO listProduct(@RequestParam Integer productCode) {
		return prodService.findByCode(productCode);		
	}
}
