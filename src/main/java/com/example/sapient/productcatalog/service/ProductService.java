package com.example.sapient.productcatalog.service;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.core.CollectionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.example.sapient.productcatalog.exception.ApplicationException;
import com.example.sapient.productcatalog.model.Item;
import com.example.sapient.productcatalog.model.Product;
import com.example.sapient.productcatalog.model.ProductDTO;
import com.example.sapient.productcatalog.repository.ItemRepository;
import com.example.sapient.productcatalog.repository.ProductRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ProductService {
	
	@Autowired
	ProductRepository repo ;
	
	@Autowired
	ItemRepository itemRepo ;

	public Integer create(Product product) {
		log.info("Product creation Started");
		if(product.getProductCode()!=null)
		{
			log.error("Create request Failed");
			throw new ApplicationException("Please do Put for updating an existing resource or don't send product code. System will automatically generate ", HttpStatus.BAD_REQUEST);
		}
		
		return repo.save(product).getProductCode();
	}
	
	public Integer saveItem(Item item) {
		log.info("Item creation Started");
		return itemRepo.save(item).getItemId();
	}
	
	public List<Product> findAllProdByCategory(String category) {
		List<Product> list = repo.findByCategory(category);
		if(org.springframework.util.CollectionUtils.isEmpty(list)) {
			throw new ApplicationException("Product Not Found", HttpStatus.NOT_FOUND);
		}
		return list;
	}
	
	
	public ProductDTO findByCode(Integer code) {
		Product p=repo.findByProductCode(code);
		ProductDTO response= ProductDTO.builder().product(p).itemList(itemRepo.findByProductCode(p.getProductCode())).build();
		return response;
	}

	public Integer update(Product productRequest) {
		if(productRequest.getProductCode()==null || repo.findByProductCode(productRequest.getProductCode())== null )
		{
			log.error("Update request Failed");
			throw new ApplicationException("Please send valid product code to update an existing record.", HttpStatus.BAD_REQUEST);
		}
		repo.save(productRequest);
		return productRequest.getProductCode();
	}
	
	

}
