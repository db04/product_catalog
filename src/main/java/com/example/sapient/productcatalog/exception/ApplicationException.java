package com.example.sapient.productcatalog.exception;

import javax.validation.constraints.NotNull;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@AllArgsConstructor
@Data
public class ApplicationException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@NotNull
	private String msg;
	@NotNull
	private HttpStatus status;
}
