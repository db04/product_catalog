package com.example.sapient.productcatalog.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import lombok.extern.slf4j.Slf4j;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
	
	@ExceptionHandler(ApplicationException.class)
	public ResponseEntity<String> handleException(ApplicationException e) {
		log.error("Exception Occured e: {}",e);
		return new ResponseEntity<>(e.getMsg(),e.getStatus());
	}
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<String> handleCheckedException(Exception e) {
		log.error("CheckedException Occured e: {}",e);
		return new ResponseEntity<>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(RuntimeException.class)
	public ResponseEntity<String> handleRuntimeException(Exception e) {
		log.error("RuntimeException Occured e: {}",e);
		return new ResponseEntity<>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
