package com.example.sapient.productcatalog.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.sapient.productcatalog.model.Item;
@Repository
public interface ItemRepository extends CrudRepository<Item, String> {

	List<Item> findByProductCode(Integer code);
}
