package com.example.sapient.productcatalog.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.sapient.productcatalog.model.Product;

@Repository
public interface ProductRepository extends CrudRepository<Product, String>  {

	List<Product> findByProductName(String name);
	Product findByProductCode(Integer code);
	List<Product> findByCategory(String name);
	List<Product> findByBrand(String name);
	List<Product> findByColor(String name);
	List<Product> findBySize(String name);
}